# Microservice de Gestion Hospitalière MedHead

Un service de gestion des données hospitalières pour simplifier les opérations hospitalières et les réservations de lits en faveur de l'entreprise MedHead.

## Configuration

- Assurez-vous d'avoir Java 17 ou une version ultérieure installée.
- Configurez les fichiers de configuration appropriés dans le dossier `src/main/resources`.
- Assurez-vous que les dépendances requises sont installées en exécutant `mvn install`.

## Construction

Pour construire le microservice, exécutez la commande suivante :

mvn clean package

## Exécution
Pour exécuter le microservice, utilisez la commande suivante :

java -jar target/service-configuration.jar
## Documentation
Pour plus d'informations sur les points de terminaison et l'utilisation du microservice, consultez la documentation ici .

## Contributions
Les contributions sont les bienvenues ! Si vous souhaitez apporter des améliorations, veuillez soumettre une pull request.

## Contact
Si vous avez des questions ou des problèmes, n'hésitez pas à nous contacter à : wiamelyadri@gmail.com

